import React from 'react';
import { LocalizationContext } from '../context/LanguageContext';
import './HomePageAuth.css';

const HomePageAuth = () => {
    const { t } = React.useContext(LocalizationContext);
    const openSignUpPage = url => {
        window.open(url, '_self', 'noopener,noreferrer');
    };
    const openSignInPage = url => {
        window.open(url, '_self', 'noopener,noreferrer');
    };

    return (
        <div className='div'>
            {t('homePage')}
            <button onClick={() => openSignInPage('http://localhost:3000/login')}>{t('login')}</button>
            <button onClick={() => openSignUpPage('http://localhost:3000/registration')}>{t('signUp')}</button>
        </div>
    );
}

export default HomePageAuth;
