import React, { useContext } from "react";
import { useNavigate } from 'react-router-dom';

import LanguageDropdown from './LanguageDropdown';
import { LocalizationContext } from '../../context/LanguageContext';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';

const Navigator = () => {
    const { t } = React.useContext(LocalizationContext);
    const navigate = useNavigate()

    const logout = () => async () => {
        localStorage.removeItem('token');
        localStorage.removeItem('refreshToken');
        navigate('/');
        navigate(0)

    };

    return (
        <Box>
            <AppBar>
            <LanguageDropdown />
                <Toolbar>
                    <Typography
                        variant='h6'
                        noWrap
                        component='div'
                        sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
                    >
                        <Link href='/' color='inherit' underline='none'>
                            LOGO
                        </Link>
                    </Typography>
                    <Typography
                        variant='h6'
                        noWrap
                        component='div'
                        sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
                    >
                        <Link href='/profile' color='inherit' underline='none'>
                            PROFIL
                        </Link>
                    </Typography>
                    <Box sx={{
                        flexGrow: 0,
                        marginLeft: 'auto',
                        '&:hover': {
                            cursor: 'pointer',
                            color: '#000045'
                        }
                    }}>
                        <Link onClick={logout()} color='inherit' underline='none'>Odjavi se</Link>
                    </Box>
                </Toolbar>
            </AppBar>
        </Box>
    );
};

export default Navigator;
