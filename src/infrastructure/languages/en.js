const eng = {
    homePage: 'Home Page',
    submit: 'Submit',
    login: 'Login',
    enterYourEmail: 'Enter your email',
    enterValidEmail: 'Enter a valid email',
    emailIsRequired: 'Email is required',
    enterYourPassword: 'Enter your password',
    passCharLength: 'Password should be of minimum 8 characters length',
    passIsRequired: 'Password is required',
    emailLabel: 'Email',
    passwordLabel: 'Password',
    usernameLabel: 'Username',
    nameLabel: 'Name',
    repasswordLabel: 'Re password',
    signUp: 'Registration',
    LogOut: 'Log Out',
    enterYourUsername: 'Enter Your username',
    usernameIsRequired: 'Username is required',
    dateOfBirthLabel: 'Date of birth',
    cityLabel: 'City',
    addressLabel: 'Address',
    phoneLabel: 'Phone',
    ownerLabel: 'Profile owner',
    createProfile: 'Create profile'
};

export default eng;
