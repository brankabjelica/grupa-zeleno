const sr = {
    homePage: 'Home pejdz',
    submit: 'Potvrdi',
    login: 'Prijava',
    enterYourEmail: 'Unesite Vaš email',
    enterValidEmail: 'Unesite ispravan email',
    emailIsRequired: 'Email je obavezan',
    enterYourPassword: 'Unesite Vašu lozinku',
    passCharLength: 'Lozinka mora da sadrži najmanje 8 karaktera',
    passIsRequired: 'Lozinka je obavezna',
    emailLabel: 'E-Pošta',
    passwordLabel: 'Lozinka',
    usernameLabel: 'Korisničko ime',
    nameLabel: 'Ime',
    repasswordLabel: 'Ponovi lozinku',
    signUp: 'Registracija',
    LogOut: 'Odjavi se',
    enterYourUsername: 'Unesite Vaše korisničko ime',
    usernameIsRequired: 'Korisničko ime je obavezno',
    dateOfBirthLabel: 'Datum rođenja',
    cityLabel: 'Grad',
    addressLabel: 'Adresa',
    phoneLabel: 'Telefon',
    ownerLabel: 'Vlasnik profila',
    createProfile: 'Kreiranje profila'
};

export default sr;
