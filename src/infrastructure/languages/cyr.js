const cyr = {
    homePage: 'Хоме пејџ',
    submit: 'Потврди',
    login: 'Пријава',
    enterYourEmail: 'Унесите Ваш емаил',
    enterValidEmail: 'Унесите исправан емаил',
    emailIsRequired: 'Емаил је обавезан',
    enterYourPassword: 'Унесите Вашу лозинку',
    passCharLength: 'Лозинка мора да садржи најмање 8 карактера',
    passIsRequired: 'Лозинка је обавезна',
    emailLabel: 'Е-Пошта',
    passwordLabel: 'Лозинка',
    usernameLabel: 'Корисничко име',
    nameLabel: 'Име',
    repasswordLabel: 'Понови лозинку',
    signUp: 'Регистрација',
    LogOut: 'Одјави се',
    enterYourUsername: 'Унесите Ваше корисничко име',
    usernameIsRequired: 'Корисничко име је обавезно',
    dateOfBirthLabel: 'Датум рођења',
    cityLabel: 'Град',
    addressLabel: 'Адреса',
    phoneLabel: 'Телефон',
    ownerLabel: 'Власник профила',
    createProfile: 'Креирање профила'
};

export default cyr;
