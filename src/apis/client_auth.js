import axios from 'axios';

export default axios.create({
    // baseURL: 'https://sudije.herokuapp.com/',
    baseURL: 'https://preqbackend.herokuapp.com/',
    headers: {
        'Content-Type': 'application/json',
    },
});