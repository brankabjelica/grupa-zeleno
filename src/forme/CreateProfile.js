import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import * as yup from 'yup';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import IconButton from '@mui/material/IconButton'
import { Container, Grid, Typography } from '@mui/material';
import Spacer from '../infrastructure/components/Spacer';
import { LocalizationContext } from '../context/LanguageContext';
import client_auth from '../apis/client_auth';
import { logo } from '../resources/images';
import { Box } from '@mui/system';
import moment from 'moment';
import client from '../apis/client'


const CreateProfile = (props) => {
    const { t } = React.useContext(LocalizationContext);
    let navigate = useNavigate()
    const [buttonState, setButtonState] = useState(false)
    const [profileData, setProfileData] = useState(props.profileData || undefined)

    useEffect(() => {
        let isActive = true;
        const loadData = async () => {

            const profile = await client.get(`/profiles/`);

            if (isActive) {
                if (profile.data.length !== 0) {
                    setProfileData(await profile.data[0])
                }
            }
        };

        if (isActive) loadData();

        return () => {
            isActive = false;
        };

    }, []);


    const initialValues = {
        date_of_birth: profileData !== undefined ? profileData.date_of_birth : moment(new Date()).format('YYYY-MM-DD'),
        city: profileData !== undefined ? profileData.city : '',
        address: profileData !== undefined ? profileData.address : '',
        phone: profileData !== undefined ? profileData.phone : ''
    }


    const postProfile = async (values) => {
        try {
            setButtonState(true)
            const user = await client.get('/auth/users/me/')
            const owner = await user.data.id

            let profile = {
                date_of_birth: moment(values.date_of_birth).format('YYYY-MM-DD'),
                city: values.city,
                address: values.address,
                phone: values.phone,
                owner: owner
            }

            if (profileData !== undefined) {

                await client.put(`/profiles/${profileData.id}/`, profile)

                alert('Uspesno ste editovali profil')

                navigate('/profile')
                navigate(0)

             }
            else {

                await client.post('/profiles/', profile)

                alert('Uspesno ste kreirali profil')

                navigate('/')
                navigate(0)
      
            }



        } catch (err) {

            if (err.response) {
                alert(JSON.stringify(err.response.data));
                setButtonState(false)
            }
        }
    };


    const validationSchema = yup.object({
        date_of_birth: yup.date()
            .required('Obavezno polje'),
        city: yup.string()
            .required('Obavezno polje'),
        address: yup.string()
            .required('Obavezno polje'),
        phone: yup.string()
            .required('Obavezno polje'),
    });


    const formik = useFormik({
        initialValues: initialValues,
        validationSchema: validationSchema,
        onSubmit: postProfile,
    });

    return (
        <div>
            {/* <button onClick={() => openHomePage('http://localhost:3000/')}>{t('homePage')}</button> */}
            <Spacer height={"5rem"} />
            <Container maxWidth="md">
                <Box
                component='img'
                sx={{
                    width: 150,
                    height: 150,
                }}
                alt='logo'
                src={logo.src}
                />
                <Spacer height={"2rem"}></Spacer>
                <Typography variant='h3' component='h3' color={'yellow'}>{t('createProfile')}</Typography>
                <Spacer height={"2rem"}></Spacer>
                <form onSubmit={formik.handleSubmit}>
                    <Grid container spacing={2} >
                        <Grid item xs={12}>
                        <TextField
                            fullWidth
                            id='date_of_birth'
                            name='date_of_birth'
                            type='date'
                            variant='outlined'
                            label={'Datum rodjenja'}
                            format='yyyy/MM/dd'
                            defaultValue={formik.values.date_of_birth}
                            selected={formik.values.date_of_birth}
                            onChange={formik.handleChange}
                            InputLabelProps={{ shrink: true, required: true }}
                            error={formik.touched.date_of_birth && Boolean(formik.errors.date_of_birth)}
                            helperText={formik.touched.date_of_birth && formik.errors.date_of_birth}
                        />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id="city"
                                name="city"
                                label={t('cityLabel')}
                                InputLabelProps={{ required: true }}
                                type="city"
                                value={formik.values.city}
                                onChange={formik.handleChange}
                                error={formik.touched.city && Boolean(formik.errors.city)}
                                helperText={formik.touched.city && formik.errors.city}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id="address"
                                name="address"
                                label={t('addressLabel')}
                                value={formik.values.address}
                                onChange={formik.handleChange}
                                error={formik.touched.address && Boolean(formik.errors.address)}
                                helperText={formik.touched.address && formik.errors.address}
                            />

                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id="phone"
                                name="phone"
                                label={t('phoneLabel')}
                                type="phone"
                                value={formik.values.phone}
                                InputLabelProps={{ required: true }}
                                onChange={formik.handleChange}
                                error={formik.touched.phone && Boolean(formik.errors.phone)}
                                helperText={formik.touched.phone && formik.errors.phone}
                            />
                        </Grid>
                        
                        <Grid item xs={12}>
                            <Button sx={{ backgroundColor: "greenyellow", color: "red", fontWeight: "bolder" }} color="primary" variant="contained" fullWidth type="submit">
                                {t('submit')}
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Container>
        </div>
    );
};


export default CreateProfile;