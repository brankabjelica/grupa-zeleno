import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import * as yup from 'yup';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import IconButton from '@mui/material/IconButton'
import { Container, Grid, Typography } from '@mui/material';
import Spacer from '../infrastructure/components/Spacer';
import { LocalizationContext } from '../context/LanguageContext';
import client_auth from '../apis/client_auth';
import { logo } from '../resources/images';
import { Box } from '@mui/system';




const SignUp = () => {
    const { t } = React.useContext(LocalizationContext);

    const navigate = useNavigate();

    const [error, setError] = React.useState('');


    const [showPassword, setShowPassword] = useState(false);
    const [showRePassword, setShowRePassword] = useState(false);

    const handleClickShowPassword = () => setShowPassword(!showPassword);
    const handleMouseDownPassword = () => setShowPassword(!showPassword);

    const handleClickShowRePassword = () => setShowRePassword(!showRePassword);
    const handleMouseDownRePassword = () => setShowRePassword(!showRePassword);

    const onSignUp = async (values) => {
        try {
            const newUser = {
                name: values.name,
                username: values.username,
                email: values.email,
                password: values.password,
                re_password: values.re_password,
            };

            console.log(newUser)

            const reg = await client_auth.post(`/auth/users/`, newUser)

            if (reg.status === 201) {
                alert('Uspesno ste se registrovali')
                navigate('/login')
                navigate(0)
            }


        } catch (error) {
            setError('Greska')
            return

        }
    }

    const validationSchema = yup.object({
        email: yup
            .string(t('enterYourEmail'))
            .email(t('enterValidEmail'))
            .required(t('emailIsRequired')),
        password: yup
            .string(t('enterYourPassword'))
            .min(8, t('passCharLength'))
            .required(t('passIsRequired')),
    });

    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        validationSchema: validationSchema,
        // onSubmit: (values) => {
        //     alert(JSON.stringify(values, null, 2));
        // },
        onSubmit: onSignUp
    });

    const openHomePage = url => {
        window.open(url, '_self', 'noopener,noreferrer');
    };

    return (
        <div>
            <button onClick={() => openHomePage('http://localhost:3000/')}>{t('homePage')}</button>
            <Spacer height={"5rem"} />
            <Container maxWidth="md">
                <Box
                component='img'
                sx={{
                    width: 150,
                    height: 150,
                }}
                alt='logo'
                src={logo.src}
                />
                <Spacer height={"2rem"}></Spacer>
                <Typography variant='h3' component='h3' color={'yellow'}>{t('signUp')}</Typography>
                <Spacer height={"2rem"}></Spacer>
                <form onSubmit={formik.handleSubmit}>
                    <Grid container spacing={2} >
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id="name"
                                name="name"
                                label={t('nameLabel')}
                                type="name"
                                value={formik.values.name}
                                onChange={formik.handleChange}
                                error={formik.touched.name && Boolean(formik.errors.name)}
                                helperText={formik.touched.name && formik.errors.name}
                            />
                        </Grid>

                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id="username"
                                name="username"
                                label={t('usernameLabel')}
                                type="username"
                                value={formik.values.username}
                                onChange={formik.handleChange}
                                error={formik.touched.username && Boolean(formik.errors.username)}
                                helperText={formik.touched.username && formik.errors.username}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id="email"
                                name="email"
                                label={t('emailLabel')}
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                error={formik.touched.email && Boolean(formik.errors.email)}
                                helperText={formik.touched.email && formik.errors.email}
                            />

                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id="password"
                                name="password"
                                label={t('passwordLabel')}
                                type={showPassword ? 'text' : 'password'}
                                value={formik.values.password}
                                onChange={formik.handleChange}
                                error={formik.touched.password && Boolean(formik.errors.password)}
                                helperText={formik.touched.password && formik.errors.password}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position='end'>
                                            <IconButton
                                                aria-label='toggle password visibility'
                                                onClick={handleClickShowPassword}
                                                onMouseDown={handleMouseDownPassword}
                                            >
                                                {showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id="re_password"
                                name="re_password"
                                label={t('repasswordLabel')}
                                type={showRePassword ? 'text' : 'password'}
                                value={formik.values.re_password}
                                onChange={formik.handleChange}
                                error={formik.touched.re_password && Boolean(formik.errors.re_password)}
                                helperText={formik.touched.re_password && formik.errors.re_password}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position='end'>
                                            <IconButton
                                                aria-label='toggle password visibility'
                                                onClick={handleClickShowRePassword}
                                                onMouseDown={handleMouseDownRePassword}
                                            >
                                                {showRePassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                        {error !== '' && <Grid item xs={12}><p style={{ color: 'red' }}>{error}</p></Grid>}
                        <Grid item xs={12}>
                            <Button sx={{ backgroundColor: "greenyellow", color: "red", fontWeight: "bolder" }} color="primary" variant="contained" fullWidth type="submit">
                                {t('submit')}
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Container>
        </div>
    );
};


export default SignUp;