import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import * as yup from 'yup';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import IconButton from '@mui/material/IconButton';
import { Container, Grid, Typography } from '@mui/material';
import Spacer from '../infrastructure/components/Spacer';
import { LocalizationContext } from '../context/LanguageContext';
import client_auth from '../apis/client_auth';






const SignIn = () => {
  const { t } = React.useContext(LocalizationContext);

  const navigate = useNavigate();

  const [error, setError] = React.useState('');

  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);


  const onSignIn = async (values) => {
    try {
      setError('');
      const User = {
        username: values.username,
        password: values.password,
      };

      console.log(User)

      const userr = await client_auth.post(`/auth/jwt/create/`, User)

      console.log(userr);
      const tok = JSON.stringify(userr.data);
      const parsedData = JSON.parse(tok);

      localStorage.setItem('token', parsedData.access);
      localStorage.setItem('refreshToken', parsedData.refresh);

      // if (userr.status === 200) {
        alert('Uspesno ste se ulogovali')
        navigate('/')
        navigate(0)
      // }


    } catch (error) {
      setError('Greska')
      return

    }
  }

  const validationSchema = yup.object({
    username: yup
      .string(t('enterYourUsername'))
      .required(t('usernameIsRequired')),
    password: yup
      .string(t('enterYourPassword'))
      .required(t('passIsRequired')),
  });

  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
    },
    validationSchema: validationSchema,
    onSubmit: onSignIn,
    // onSubmit: (values) => {
    //   alert(JSON.stringify(values, null, 2));
    // },
  });

  const openHomePage = url => {
    window.open(url, '_self', 'noopener,noreferrer');
  };
  const openSignUpPage = url => {
    window.open(url, '_self', 'noopener,noreferrer');
  };

  return (
    <div>
      <button onClick={() => openHomePage('http://localhost:3000/')}>{t('homePage')}</button>
      <button onClick={() => openSignUpPage('http://localhost:3000/registration')}>{t('signUp')}</button>
      <Spacer height={"5rem"} />
      <Container maxWidth="md">
        <Typography variant='h3' component='h3' color={'yellow'}>{t('login')}</Typography>
        <Spacer height={"2rem"}></Spacer>
        <form onSubmit={formik.handleSubmit}>
          <Grid container spacing={2} >
            <Grid item xs={12}>
              <TextField
                fullWidth
                id="username"
                name="username"
                label={t('usernameLabel')}
                value={formik.values.username}
                onChange={formik.handleChange}
                error={formik.touched.username && Boolean(formik.errors.username)}
                helperText={formik.touched.username && formik.errors.username}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                id="password"
                name="password"
                label={t('passwordLabel')}
                type={showPassword ? 'text' : 'password'}
                value={formik.values.password}
                onChange={formik.handleChange}
                error={formik.touched.password && Boolean(formik.errors.password)}
                helperText={formik.touched.password && formik.errors.password}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position='end'>
                      <IconButton
                        aria-label='toggle password visibility'
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  )
                }}
              />
            </Grid>
            {error !== '' && <Grid item xs={12}><p style={{ color: 'red' }}>{error}</p></Grid>}
            <Grid item xs={12}>
              <Button sx={{ backgroundColor: "greenyellow", color: "red", fontWeight: "bolder" }} color="primary" variant="contained" fullWidth type="submit">
                {t('submit')}
              </Button>
            </Grid>
          </Grid>
        </form>
      </Container>
    </div>
  );
};


export default SignIn;